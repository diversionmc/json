package net.thebugmc.json;

import net.thebugmc.error.Functionals;
import net.thebugmc.error.Result;
import net.thebugmc.error.TryS;
import net.thebugmc.parser.Parser;
import net.thebugmc.parser.expression.*;
import net.thebugmc.parser.pattern.PatternResult;
import net.thebugmc.parser.pattern.Sentence;
import net.thebugmc.parser.util.FilePointer;
import net.thebugmc.parser.util.ParserException;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.Map.Entry;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.stream.Stream;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.util.Map.entry;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toMap;
import static net.thebugmc.error.Functionals.*;
import static net.thebugmc.error.Result.Error;
import static net.thebugmc.error.Result.Ok;
import static net.thebugmc.parser.expression.StringPiece.escape;
import static net.thebugmc.parser.pattern.ParsePattern.matchOne;
import static net.thebugmc.parser.util.ParserException.ASSERT;

/**
 * Simple {@link Parser} example which parses JSON files into {@link JSONValue} objects.
 *
 * @author Kirill Semyonkin - Diversion Network 2021
 */
public class JSON {
    //
    // Expression Pieces
    //

    private static class ObjectStart extends CharPiece {
        public ObjectStart(FilePointer ptr) {
            super(ptr, '{');
        }
    }

    private static class ObjectEnd extends CharPiece {
        public ObjectEnd(FilePointer ptr) {
            super(ptr, '}');
        }
    }

    private static class ObjectGroup extends GroupPiece {
        public ObjectGroup(FilePointer ptr, List<ExpressionPiece> content) {
            super(ptr, content);
        }
    }

    private static class ArrayStart extends CharPiece {
        public ArrayStart(FilePointer ptr) {
            super(ptr, '[');
        }
    }

    private static class ArrayEnd extends CharPiece {
        public ArrayEnd(FilePointer ptr) {
            super(ptr, ']');
        }
    }

    private static class ArrayGroup extends GroupPiece {
        public ArrayGroup(FilePointer ptr, List<ExpressionPiece> content) {
            super(ptr, content);
        }
    }

    private static class MemberPiece extends CharPiece {
        public MemberPiece(FilePointer ptr) {
            super(ptr, ':');
        }
    }

    private static class Separator extends CharPiece {
        public Separator(FilePointer ptr) {
            super(ptr, ',');
        }
    }

    //
    // JSON Objects
    //

    /**
     * Parsed JSON value that stores any data.
     */
    public abstract static sealed class JSONValue extends Sentence
        permits JSONObject, JSONArray, JSONNumber, JSONString {

        /**
         * Construct a JSON Value.
         *
         * @param ptr Creation position.
         */
        public JSONValue(FilePointer ptr) {
            super(ptr);
        }

        /**
         * Convert this value to {@link JSONObject}.
         */
        public Optional<JSONObject> object() {
            return this instanceof JSONObject j ? some(j) : none();
        }

        /**
         * Convert this value to {@link JSONArray}.
         */
        public Optional<JSONArray> array() {
            return this instanceof JSONArray j ? some(j) : none();
        }

        /**
         * Convert this value to {@link JSONNumber}.
         */
        public Optional<JSONNumber> number() {
            return this instanceof JSONNumber j ? some(j) : none();
        }

        /**
         * Convert this value to {@link JSONString}.
         */
        public Optional<JSONString> string() {
            return this instanceof JSONString j ? some(j) : none();
        }
    }

    /**
     * Parsed JSON object that stores key-{@link JSONValue} pairs.
     */
    public static final class JSONObject extends JSONValue {
        private final Map<String, JSONValue> values;

        /**
         * Construct a JSON Object {"key":value,...}
         *
         * @param ptr    Creation position.
         * @param values Values to store.
         */
        public JSONObject(FilePointer ptr, Map<String, JSONValue> values) {
            super(ptr);
            this.values = values;
        }

        /**
         * Serialize current JSONObject back to JSON.
         *
         * @return {"key":value,...}
         */
        public String toString() {
            return "{" + values.entrySet().stream()
                .map(e -> "\"" + escape(e.getKey()) + "\":" + e.getValue())
                .collect(joining(",")) + "}";
        }

        /**
         * Get all values stored in this object.
         *
         * @return key-{@link JSONValue} pairs
         */
        public Map<String, JSONValue> toMap() {
            return values;
        }

        /**
         * Perform an action for each value in this object.
         *
         * @param action Action to perform.
         */
        public void forEach(BiConsumer<String, JSONValue> action) {
            values.forEach(action);
        }

        /**
         * Find a value by a deep path.
         *
         * @param path Path to value. Empty path gives optional of this JSONValue.
         * @return Found value.
         */
        public Optional<JSONValue> find(String... path) {
            var opt = Functionals.<JSONValue>some(this);
            for (var t : path)
                opt = opt
                    .flatMap(JSONValue::object)
                    .flatMap(c -> optional(c.values.get(t)));
            return opt;
        }

        /**
         * Find a value of type {@link JSONObject}.
         *
         * @param path Path to value. Empty path gives optional of this JSONValue converted back
         *             to object.
         */
        public Optional<JSONObject> object(String... path) {
            return find(path).flatMap(JSONValue::object);
        }

        /**
         * Find a value of type {@link JSONArray}.
         *
         * @param path Path to value. Empty path gives optional of this JSONValue converted to
         *             array (so empty).
         */
        public Optional<JSONArray> array(String... path) {
            return find(path).flatMap(JSONValue::array);
        }

        /**
         * Find a value of type {@link JSONNumber}.
         *
         * @param path Path to value. Empty path gives optional of this JSONValue converted to
         *             number (so empty).
         */
        public Optional<JSONNumber> number(String... path) {
            return find(path).flatMap(JSONValue::number);
        }

        /**
         * Find a value of type {@link JSONString}.
         *
         * @param path Path to value. Empty path gives optional of this JSONValue converted to
         *             string (so empty).
         */
        public Optional<JSONString> string(String... path) {
            return find(path).flatMap(JSONValue::string);
        }
    }

    /**
     * Parsed JSON array that stores a list of {@link JSONValue}.
     */
    public static final class JSONArray extends JSONValue {
        private final List<JSONValue> values;

        /**
         * Construct a JSON Array [value,...]
         *
         * @param ptr    Creation position.
         * @param values Values to store.
         */
        public JSONArray(FilePointer ptr, List<JSONValue> values) {
            super(ptr);
            this.values = values;
        }

        /**
         * Serialize current JSONArray back to JSON.
         *
         * @return [value, ...]
         */
        public String toString() {
            return "[" + values.stream()
                .map(Object::toString)
                .collect(joining(",")) + "]";
        }

        /**
         * Get all values stored in this array.
         *
         * @return {@link JSONValue} list.
         */
        public List<JSONValue> toList() {
            return values;
        }

        /**
         * Get a stream of values of this array.
         *
         * @return Value stream.
         */
        public Stream<JSONValue> stream() {
            return values.stream();
        }

        /**
         * Perform an action for each value in this array.
         *
         * @param action Action to perform.
         */
        public void forEach(Consumer<JSONValue> action) {
            values.forEach(action);
        }
    }

    /**
     * Parsed JSON string, or stored values true, false or null.
     */
    public static final class JSONString extends JSONValue {
        private final String s;

        /**
         * Construct a JSON String "text..."
         *
         * @param ptr Creation position.
         * @param s   String content to store.
         */
        public JSONString(FilePointer ptr, String s) {
            super(ptr);
            this.s = s == null ? "null" : s;
        }

        /**
         * Serialize current JSONArray back to JSON.
         *
         * @return true, false, null, or "text,..."
         */
        public String toString() {
            return "true".equals(s)
                   || "false".equals(s)
                   || "null".equals(s)
                ? s
                : "\"" + escape(s) + "\"";
        }

        /**
         * Get content of this JSONString.
         *
         * @return String content.
         */
        public String content() {
            return s;
        }
    }

    /**
     * Parsed JSON number, as a double value.
     */
    public static final class JSONNumber extends JSONValue {
        private final double v;

        /**
         * Construct a JSON Number.
         *
         * @param ptr Creation position.
         * @param v   Number to store.
         */
        public JSONNumber(FilePointer ptr, double v) {
            super(ptr);
            this.v = v;
        }

        /**
         * Serialize number back to JSON.
         *
         * @return {@link #number()}
         */
        public String toString() {
            return v + "";
        }

        /**
         * Get content of this JSONNumber.
         *
         * @return Double value.
         */
        public double doubleValue() {
            return v;
        }
    }

    //
    // API
    //

    /**
     * Convert an arbitrary object into JSON.
     * It has to be a {@link Map}, {@link Collection}, {@link String}, null, true, false or a
     * number.
     *
     * @param object Object to serialize.
     * @return Serialized object.
     */
    public static Result<JSONValue, ParserException> of(Object object) {
        var ptr = new FilePointer("Generated JSON " + new Object().hashCode(), 1, 1);

        // map
        if (object instanceof Map<?, ?> m) {
            if (m.entrySet().stream()
                .anyMatch(e -> !(e.getKey() instanceof String)))
                return Error(new ParserException(
                    "Cannot convert map that contains non-string keys to JSON"));
            var entries = m.entrySet().stream()
                .map(e -> entry((String) e.getKey(), of(e.getValue())))
                .toList();
            return entries.stream()
                .flatMap(e -> e.getValue().error().stream())
                .findFirst()
                .<Result<JSONValue, ParserException>>map(Result::Error)
                .orElseGet(() -> Ok(new JSONObject(
                    ptr,
                    entries.stream()
                        .collect(toMap(
                            Entry::getKey,
                            e -> e.getValue().ok().get()
                        ))
                )));
        }

        // collection
        if (object instanceof Collection<?> c) {
            var results = c.stream()
                .map(JSON::of)
                .toList();
            return results.stream()
                .flatMap(r -> r.error().stream())
                .findFirst()
                .<Result<JSONValue, ParserException>>map(Result::Error)
                .orElseGet(() -> Ok(new JSONArray(
                    ptr,
                    results.stream()
                        .flatMap(r -> r.ok().stream())
                        .toList()
                )));
        }

        // string
        if (object instanceof String s)
            return Ok(new JSONString(ptr, s));

        // bool/null
        if (object == null
            || object == TRUE
            || object == FALSE)
            return Ok(new JSONString(ptr, object + ""));

        // number
        if (object instanceof Number n)
            return Ok(new JSONNumber(ptr, n.doubleValue()));

        // other
        return Error(new ParserException("Cannot convert to JSON an object " + object));
    }

    /**
     * Convert a file into a JSONValue.
     *
     * @param json JSON file to parse.
     * @return Parsed JSON storage.
     */
    public static Result<JSONValue, IOException> from(File json) {
        return Result.tryGet(
            IOException.class,
            (TryS<JSONValue, IOException>)
                () -> parse(new Parser<JSONValue>().readFrom(json))
        );
    }

    /**
     * Convert a data stream into a JSONValue.
     *
     * @param json JSON data stream to parse.
     * @return Parsed JSON storage.
     */
    public static Result<JSONValue, IOException> from(InputStream json) {
        return Result.tryGet(
            IOException.class,
            (TryS<JSONValue, IOException>)
                () -> parse(new Parser<JSONValue>().readFrom(json))
        );
    }

    /**
     * Convert a string into a JSONValue.
     *
     * @param json String to parse.
     * @return Parsed JSON storage.
     */
    public static Result<JSONValue, IOException> from(String json) {
        return Result.tryGet(
            IOException.class,
            (TryS<JSONValue, IOException>)
                () -> parse(Parser.<JSONValue>parser().text(json))
        );
    }

    private static JSONValue parse(Parser<JSONValue> parser) {
        var json = parser
            .piece((c, ptr) -> c == '{', (c, ptr) -> new ObjectStart(ptr))
            .piece((c, ptr) -> c == '}', (c, ptr) -> new ObjectEnd(ptr))
            .piece((c, ptr) -> c == '[', (c, ptr) -> new ArrayStart(ptr))
            .piece((c, ptr) -> c == ']', (c, ptr) -> new ArrayEnd(ptr))
            .piece((c, ptr) -> c == ':', (c, ptr) -> new MemberPiece(ptr))
            .piece((c, ptr) -> c == ',', (c, ptr) -> new Separator(ptr))
            .piece((c, ptr) -> c == '"', (c, ptr) -> new StringPiece(ptr, c))
            .piece((c, ptr) -> c >= '0' && c <= '9' || c == '-', (c, ptr) -> new NumberPiece(ptr))
            .piece((c, ptr) -> c == 't' || c == 'f' || c == 'n', (c, ptr) -> new NamePiece(ptr, ""))
            .group(
                e -> e instanceof ObjectStart,
                e -> e instanceof ObjectEnd,
                (left, right, content) -> new ObjectGroup(left.pointer(), content)
            )
            .group(
                e -> e instanceof ArrayStart,
                e -> e instanceof ArrayEnd,
                (left, right, content) -> new ArrayGroup(left.pointer(), content)
            )
            .pattern("object", e -> { // <object([<”key”><:><...>[<,><”key”><:><...>]...])>
                if (!(e.getFirst() instanceof ObjectGroup g)) return null;

                e = g.content();
                int pos = 0, limit = e.size();

                var res = new HashMap<String, JSONValue>();
                while (pos < limit) {
                    if (!res.isEmpty() && !(e.get(pos++) instanceof Separator)) return null;
                    if (!(e.get(pos++) instanceof StringPiece key)) return null;
                    if (!(e.get(pos++) instanceof MemberPiece)) return null;

                    var r = matchOne(e.subList(pos, limit), parser.patterns());
                    if (r == null) return null;
                    pos += r.length();

                    res.put(key.content(), r.result());
                }
                return new PatternResult<>(1, new JSONObject(g.pointer(), res));
            })
            .pattern("array", e -> { // <array([<...>[<,><...>]...])>
                if (!(e.getFirst() instanceof ArrayGroup g)) return null;

                e = g.content();
                int pos = 0, limit = e.size();

                var res = new ArrayList<JSONValue>();
                while (pos < limit) {
                    if (!res.isEmpty() && !(e.get(pos++) instanceof Separator)) return null;

                    var r = matchOne(e.subList(pos, limit), parser.patterns());
                    if (r == null) return null;
                    pos += r.length();

                    res.add(r.result());
                }

                return new PatternResult<>(1, new JSONArray(g.pointer(), res));
            })
            .pattern("string", e -> { // <”string”>
                if (!(e.getFirst() instanceof StringPiece s)) return null;
                return new PatternResult<>(1, new JSONString(s.pointer(), s.content()));
            })
            .pattern("number", e -> { // <number>
                if (!(e.getFirst() instanceof NumberPiece n)) return null;
                return new PatternResult<>(1, new JSONNumber(n.pointer(), n.number()));
            })
            .pattern("other", e -> { // <true|false|null>
                if (!(e.getFirst() instanceof NamePiece s)) return null;

                var res = s.name();
                if (!"true".equals(res)
                    && !"false".equals(res)
                    && !"null".equals(res)) return null;

                return new PatternResult<>(1, new JSONString(s.pointer(), s.name()));
            })
            .build();
        ASSERT(
            json.size() == 1,
            () -> json.isEmpty()
                ? new FilePointer(parser.name(), 1, 1)
                : json.getLast().pointer(),
            "Found extra information in the JSON string"
        );
        return json.getFirst();
    }
}
